import PySimpleGUI as sg
import urllib3
import shutil
from bs4 import BeautifulSoup
from pathlib import Path

base_url = "http://ezshare.card/"
start_page = "dir?dir=A:"
start_link = base_url+start_page
http = urllib3.PoolManager()
been = []

def get_file(directory):
    been.append(directory)
    r = http.request('GET', directory)
    if r.status == 200:
        soup = BeautifulSoup(r.data, 'html.parser')
        links = soup.find_all('a')
        for link in links:
            if str.startswith(link.get('href'), "http"):
                folder = link.get('href').split("file=")[1].split("%5C")
                folder.insert(0, "SDCard")
                folder[-1] = link.contents[0].strip()
                folderpath = Path('/'.join(folder[:-1]))
                # print("folder", '/'.join(folder[:-1]))
                filename = './'+'/'.join(folder)
                if len(folder)>1:
                    Path.mkdir(folderpath, parents=True, exist_ok=True)
                with http.request('GET', link.get('href'), preload_content=False) as res, open(filename, 'wb') as out_file:
                    shutil.copyfileobj(res, out_file)
            if str.startswith(link.get('href'), "dir") and base_url + link.get('href') not in been:
                # print("directory", base_url + link.get('href'))
                get_file(base_url + link.get('href'))
                

sg.theme('Topanga')

layout = [
    [sg.Text('Apnea Scan')],
    [sg.Button('Fetch Data')]
]

window = sg.Window("Apnea Scan", layout, finalize=True)

while True:
    event, values = window.read()
    print(event, values)
    if event in (sg.WIN_CLOSED, 'Exit'):
        break
    if event == "Fetch Data":
        get_file(start_link)
        sg.popup("Finished Data is In the SDCard folder")

window.close()